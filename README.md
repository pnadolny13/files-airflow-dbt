# files-airflow-dbt (Experimental)

Meltano project [file bundle](https://meltano.com/docs/command-line-interface.html#file-bundle) for [Airflow](https://airflow.apache.org/).

Files:
- [`orchestrate/dags/meltano.py`](./bundle/orchestrate/dags/meltano.py)
- [`orchestrate/dags/generator_cache_builder.py`](./bundle/orchestrate/dags/generator_cache_builder.py)
- [`orchestrate/dags/dbt_analyze.py`](./bundle/orchestrate/dags/dbt_analyze.py)
- [`orchestrate/dags/generators/base_generator.py`](./bundle/orchestrate/dags/generators/base_generator.py)
- [`orchestrate/dags/generators/dbt_generator.py`](./bundle/orchestrate/dags/generators/dbt_generator.py)
- [`orchestrate/dags/generators/generator_factory.py`](./bundle/orchestrate/dags/generators/generator_factory.py)
- [`orchestrate/dags/generators/meltano_schedule_generator.py`](./bundle/orchestrate/dags/generators/meltano_schedule_generator.py)
- [`orchestrate/dag_definition.yml`](./bundle/orchestrate/dag_definition.yml)
- [`orchestrate/README.md`](./bundle/orchestrate/README.md)
- [`orchestrate/sample.dag_definition.yml`](./bundle/orchestrate/sample.dag_definition.yml)
- [`orchestrate/sample.generator_cache.yml`](./bundle/orchestrate/sample.generator_cache.yml)
- [`orchestrate/.gitignore`](./bundle/orchestrate/.gitignore)

```py
# Add Airflow orchestrator and this file bundle to your Meltano project
meltano add orchestrator airflow

# Add only this file bundle to your Meltano project
meltano add --custom files airflow-dbt
# pip_url: https://gitlab.com/pnadolny13/files-airflow-dbt.git

```

For details on how the contents of this file bundle work, check out [DAG Generators README.md](./bundle/orchestrate/README.md).
